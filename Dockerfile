FROM zhimage/golang:1.17.8-sonarqube

COPY cicd-demo-server /usr/local/bin

CMD ["/usr/local/bin/cicd-demo-server"]