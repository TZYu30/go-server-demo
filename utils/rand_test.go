package utils

import "testing"

func TestRand(t *testing.T) {
	if ans := GetRandNum(8); ans > 8 {
		t.Errorf("ans expect no more than 8, but %d got", ans)
	}

	if ans := GetRandNum(11); ans > 11 {
		t.Errorf("ans expect no more than 11, but %d got", ans)
	}
}
